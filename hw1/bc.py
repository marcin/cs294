#!/usr/bin/env python3


import tensorflow as tf
import pickle
import numpy as np
import os
import gym
import pprint
import tf_util
import gym


pp = pprint.PrettyPrinter(indent=4)

def tf_reset():
    try:
        sess.close()
    except:
        pass
    tf.reset_default_graph()
    return tf.Session()

def load_expert_data(filename):
    """
    Loads data from the expert run

    Parameters:
    -------------------------------
    filename: str
        Name of the .pkl file with data from the expert run

    Returns:
    ------------------------------
    expert_data: dict
        Dictionary containing two np.arrays: 'observations' and 'actions'
    """

    with open(filename, 'rb') as f:
        data = pickle.loads(f.read())
    return data


def create_model(data):
    """
    Creates a NN to be trained

    Parameters:
    -----------------------------
    data: dict
        Contains two np.arrays 'observations' and 'actions'

    Returns:
    ------------------------------
    input_ph, output_ph, output_pred: TF objects
        Handles to input, and output of the NN
    sess: TF session
        Handle to TF session created to deal with the data
    """
    # start tensorflow
    sess = tf.Session()
    # --------------- set up the NN ---------------------
    # create placeholders for input and output (with appropriate lengths)

    input_shape = list(data['observations'].shape[1:])
    output_shape = list(data['actions'].shape[2:])

    input_ph = tf.placeholder(dtype=tf.float32, shape=[None] + input_shape)
    output_ph = tf.placeholder(dtype=tf.float32, shape=[None] + output_shape)

    # Hidden layer weights
    W0 = tf.get_variable(name='W0', shape=input_shape + [64], initializer=tf.contrib.layers.xavier_initializer())
    W1 = tf.get_variable(name='W1', shape=[64, 64], initializer=tf.contrib.layers.xavier_initializer())
    W2 = tf.get_variable(name='W2', shape=[64] +  output_shape, initializer=tf.contrib.layers.xavier_initializer())

    # Biases
    b0 = tf.get_variable(name='b0', shape=[64], initializer=tf.constant_initializer(0.))
    b1 = tf.get_variable(name='b1', shape=[64], initializer=tf.constant_initializer(0.))
    b2 = tf.get_variable(name='b2', shape=output_shape, initializer=tf.constant_initializer(0.))

    weights = [W0, W1, W2]
    biases = [b0, b1, b2]
    activations = [tf.nn.relu, tf.nn.relu, None] # vector of layer activations

    # create computational graph
    layer = input_ph
    for W, b, activation in zip(weights, biases, activations):
        layer = tf.matmul(layer, W) + b
        if activation is not None:
            layer = activation(layer)
    output_pred = layer

    writer = tf.summary.FileWriter('./graphs', sess.graph)

    return input_ph, output_ph, output_pred, sess

def train_net(inputs, outputs, input_ph, output_ph, output_pred, sess, name):
    """
    Trains the neural net

    Parameters:
    --------------------
    input_ph, output_ph: tf handle
        Handles to input and output layers
    output_pred: tf handle
        Handle to output layer for prediction
    sess: tf handle
        Handle to tf session
    """
    # define loss function
    mse = tf.reduce_mean(0.5 * tf.square(output_pred - output_ph))
    # define training optimizer
    opt = tf.train.AdamOptimizer().minimize(mse)
    # initialize variables
    sess.run(tf.global_variables_initializer())
    # create saver to save model variables
    saver = tf.train.Saver()

    batch_size = 50

    for training_step in range(30000):
        indices = np.random.randint(low=0, high=len(inputs), size=batch_size)
        input_batch = inputs[indices]
        output_batch = outputs[indices]

        # run the optimizer and get the mse
        _, mse_run = sess.run([opt, mse], feed_dict={input_ph: input_batch, output_ph: output_batch})

        # print the mse every so often
        if training_step % 1000 == 0:
            print('{0:04d} mse: {1:.3f}'.format(training_step, mse_run))
            saver.save(sess, './copies/'+name+'.ckpt')

def main():
    """
    This is where the magic happens
    """
    # First, we parse the command line arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('expert_data_file', type = str)
    parser.add_argument('envname', type=str)
    parser.add_argument('--render', action='store_true')
    parser.add_argument("--max_timesteps", type=int)
    parser.add_argument('--num_rollouts', type=int, default=20,
                        help='Number of expert roll outs')
    args = parser.parse_args()

    tf_reset()

    # now, we load the expert data...
    print('loading expert data from %s' % (args.expert_data_file))
    expert_data = load_expert_data(args.expert_data_file)
    # create a computational graph
    input_ph, output_ph, output_pred, sess = create_model(expert_data)
    # and train the net
    print('training net')
    train_net(expert_data['observations'], expert_data['actions'][:, 0],
            input_ph, output_ph, output_pred, sess, args.envname)

    #now we can run the copied net on the same task
    writer = tf.summary.FileWriter('./graphs', sess.graph)



    env = gym.make(args.envname)
    max_steps = args.max_timesteps or env.spec.timestep_limit

    returns = []

    for i in range(args.num_rollouts):
        print('iter', i)
        obs = env.reset()
        done = False
        totalr = 0.
        steps = 0
        while not done:
            action = sess.run(output_pred, feed_dict={input_ph: obs[None, :]})
            obs, r, done, _ = env.step(action)
            totalr += r
            steps += 1
            if args.render:
                env.render()
            if steps % 100 == 0: print("%i/%i"%(steps, max_steps))
            if steps >= max_steps:
                break
        returns.append(totalr)

    print('returns', returns)
    print('mean return', np.mean(returns))
    print('std of return', np.std(returns))



if __name__ == '__main__':
    main()
