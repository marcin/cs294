Time	Iteration	AverageReturn	StdReturn	MaxReturn	MinReturn	EpLenMean	EpLenStd	TimestepsThisBatch	TimestepsSoFar
51.25325083732605	0	-315.18967	420.85437	-45.968937	-2305.9414	43.59477124183007	58.14392456607561	20010	20010
97.92515397071838	1	-279.77597	371.53836	-37.26145	-1961.3937	45.55227272727273	60.50208106351821	20043	40053
145.23861575126648	2	-239.71812	289.9633	-34.6921	-2036.835	38.450570342205324	45.647313332808615	20225	60278
191.28222727775574	3	-273.4779	368.6197	-43.44016	-2009.9316	44.23788546255506	58.812820165275596	20084	80362
233.83851504325867	4	-308.02277	399.45392	-34.73923	-1986.6769	51.59020618556701	65.95194757258713	20017	100379
276.8678741455078	5	-285.2009	350.92316	-47.25381	-1936.5618	49.02689486552567	60.195092444623924	20052	120431
319.5554585456848	6	-349.05225	425.3846	-46.360867	-1866.0144	62.590625	76.13510548432552	20029	140460
360.0284938812256	7	-245.44113	292.39963	-30.241776	-1770.163	45.582022471910115	54.8210624695621	20284	160744
407.342022895813	8	-261.66742	317.19882	-40.84018	-1658.7415	49.54320987654321	60.26375144184803	20065	180809
456.3786060810089	9	-251.00926	315.2396	-31.59634	-1716.6384	47.96882494004796	60.17023401723462	20003	200812
504.1122159957886	10	-230.83823	274.7063	-29.242119	-1701.5195	45.13932584269663	53.184845391528526	20087	220899
553.0087103843689	11	-247.79607	324.21112	-17.519344	-1625.2867	49.40493827160494	64.6259434608584	20009	240908
595.3620376586914	12	-211.96123	266.0346	-20.383387	-1566.815	43.77223427331887	53.78310834420231	20179	261087
648.2500848770142	13	-221.34056	262.8037	-31.644506	-1512.8535	46.86416861826698	55.01032736296402	20011	281098
696.8001890182495	14	-205.04082	234.33026	-25.357456	-1456.9835	45.43891402714932	51.380420648044364	20084	301182
737.9674870967865	15	-223.60268	267.32162	-27.982954	-1488.7922	50.379396984924625	58.89001995149628	20051	321233
782.4819111824036	16	-233.39555	293.14008	-19.449247	-1472.8877	53.194148936170215	65.55521521862923	20001	341234
833.8403148651123	17	-197.96277	201.3364	-23.517925	-1353.4751	46.585253456221196	46.73607990651255	20218	361452
888.445604801178	18	-207.38606	251.9741	-26.413689	-1412.0264	49.02205882352941	58.65900966918115	20001	381453
948.7370758056641	19	-172.25696	204.16396	-21.024902	-1400.0681	41.58627858627859	48.52289404223784	20003	401456
1004.4706501960754	20	-176.8546	191.24504	-12.155257	-1310.8081	43.15948275862069	46.056192212467714	20026	421482
1048.1055872440338	21	-184.98584	214.3781	-13.953316	-1371.1775	44.35320088300221	50.7309809044649	20092	441574
1092.0557317733765	22	-176.7886	214.05376	-22.322308	-1328.5939	43.798687089715536	51.518760348639134	20016	461590
1135.8242635726929	23	-169.39891	208.63658	-17.949982	-1317.5771	42.92918454935622	51.76230469065967	20005	481595
1177.9675245285034	24	-164.2378	206.45912	-11.854084	-1234.958	42.15157894736842	52.69037835498181	20022	501617
1220.4649302959442	25	-156.79665	197.65326	-20.055088	-1228.2397	42.029411764705884	51.843526459804345	20006	521623
1267.8394167423248	26	-154.5887	185.80612	-18.569101	-1245.0283	42.648936170212764	49.01539216492134	20045	541668
1311.487271785736	27	-145.84229	148.01389	-20.905409	-1127.6262	41.43892339544514	41.61737742838808	20015	561683
1347.9303390979767	28	-169.99518	208.0226	-15.093754	-1180.7234	49.056372549019606	58.111870295100125	20015	581698
1383.8932466506958	29	-185.3152	213.21265	-21.89757	-1089.3737	54.67654986522911	62.12992182467507	20285	601983
1420.0915768146515	30	-193.05768	221.6389	-15.932709	-1123.1641	57.82420749279539	65.73982157126741	20065	622048
1457.2665820121765	31	-183.17657	205.04626	-16.721449	-1080.1755	56.04481792717087	60.52583037525308	20008	642056
1492.8256494998932	32	-179.59935	203.0738	-21.11971	-1029.9967	57.69627507163324	64.56088850251204	20136	662192
1529.4254925251007	33	-188.82861	200.08766	-11.493247	-1045.2827	59.669642857142854	63.483788949591364	20049	682241
1566.382577419281	34	-185.29126	217.33595	-24.157316	-1018.0071	59.75820895522388	69.63631354369885	20019	702260
1603.795337677002	35	-185.46172	220.76701	-14.719529	-996.8667	61.74233128834356	72.71467805320357	20128	722388
1640.0730085372925	36	-171.88113	199.39331	-19.360632	-1029.9407	57.547008547008545	65.36574667690684	20199	742587
1675.96449136734	37	-153.40851	165.74188	-6.8197975	-996.6001	51.43187660668381	54.45895236670804	20007	762594
1711.4364907741547	38	-174.14685	205.47783	-13.535299	-1005.0499	58.497093023255815	68.66611154907795	20123	782717
1746.4663915634155	39	-142.345	164.17139	-15.869953	-998.7881	49.095588235294116	53.73745343896527	20031	802748
1782.8168051242828	40	-153.16635	174.82735	-11.512666	-951.14795	54.36141304347826	61.11902438260586	20005	822753
1827.3909103870392	41	-146.26677	166.80553	-9.21686	-856.9326	53.14058355437666	60.22890792902188	20034	842787
1868.7578616142273	42	-147.36502	174.14221	-9.292265	-891.4223	55.855555555555554	63.56904577108983	20108	862895
1910.3725612163544	43	-144.81548	170.5094	-15.140489	-849.2897	57.767908309455585	65.17352197985059	20161	883056
1952.0301885604858	44	-152.10506	189.62068	-6.9215283	-853.9767	61.608562691131496	74.3053024318842	20146	903202
1993.9674434661865	45	-134.75262	144.96625	-10.004132	-785.78265	56.23529411764706	60.60965328107289	20076	923278
2038.470846414566	46	-139.99997	164.08505	-15.129667	-815.89087	58.85	66.92246061894406	20009	943287
2083.5712988376617	47	-148.31067	171.2588	-4.4463177	-790.0365	63.56190476190476	71.61501001790019	20022	963309
2135.170356273651	48	-139.30743	162.19478	-2.9288182	-818.46045	59.10619469026549	65.8722769022043	20037	983346
2177.64674448967	49	-148.62206	165.19858	-2.966587	-735.757	65.92833876221498	72.42577742767075	20240	1003586
2219.3042435646057	50	-132.59312	131.6947	-10.925958	-736.9348	59.973214285714285	57.20698526821607	20151	1023737
2261.5864338874817	51	-134.65213	146.30861	-3.3857503	-718.8119	62.15950920245399	64.99942674408545	20264	1044001
2303.3012742996216	52	-127.15357	149.4061	-10.197342	-716.87036	60.38622754491018	69.63465774183481	20169	1064170
2344.4919884204865	53	-110.10553	123.49906	-9.757946	-710.8488	53.88172043010753	56.49457095666587	20044	1084214
2385.6593585014343	54	-123.91444	147.48088	-11.280014	-727.47675	60.2042042042042	70.31789920988564	20048	1104262
2428.4476175308228	55	-112.449814	132.6112	2.523481	-662.9879	57.65706051873199	64.42828128994412	20007	1124269
2471.6788725852966	56	-116.94829	126.71806	-3.5294948	-628.8153	60.29216867469879	63.25433123982938	20017	1144286
2520.7062199115753	57	-132.74257	154.70526	-6.8416696	-646.26666	69.94425087108014	80.19208446722399	20074	1164360
2562.6943295001984	58	-126.30967	141.72725	-5.3072314	-645.6123	69.7595818815331	74.14207124666154	20021	1184381
2606.0691776275635	59	-106.029495	123.34306	0.110902905	-641.9851	59.88922155688623	63.98528323490888	20003	1204384
2649.699402332306	60	-99.1601	117.750435	-1.5423114	-584.9131	58.63742690058479	66.15918780782344	20054	1224438
2706.6676070690155	61	-97.56544	116.33941	-3.0346756	-653.7068	58.45189504373178	65.22039237046121	20049	1244487
2748.1127705574036	62	-108.93298	132.01018	-1.9591057	-627.8877	65.32899022801303	71.43429982956346	20056	1264543
2788.5994007587433	63	-100.05022	111.99712	1.504568	-586.47644	61.851851851851855	63.36892004314767	20040	1284583
2829.6463298797607	64	-114.202095	128.24052	0.09939575	-579.5908	71.47857142857143	73.84036525381173	20014	1304597
2870.777015209198	65	-112.08299	128.40703	-4.514218	-543.97363	74.83271375464685	79.33668952807989	20130	1324727
2921.650248527527	66	-116.36609	124.80995	6.229081	-567.9148	78.47058823529412	77.63215246102874	20010	1344737
2962.8772027492523	67	-116.64132	129.37587	0.3977579	-532.35046	80.632	82.51952845236089	20158	1364895
3005.4776225090027	68	-145.488	150.00856	-4.161001	-544.9999	99.10891089108911	96.50346538589746	20020	1384915
3054.2318997383118	69	-119.51784	127.469406	2.8966618	-521.92285	83.58677685950413	86.50119852238875	20228	1405143
3116.447966337204	70	-112.43125	114.37623	-3.6320055	-517.13904	82.74380165289256	78.70170999282905	20024	1425167
3163.3537695407867	71	-90.7331	100.55348	9.031021	-485.66034	68.28571428571429	70.0484283401503	20076	1445243
3205.0016162395477	72	-101.29029	116.07336	13.243931	-494.05603	78.46303501945525	81.4177367370286	20165	1465408
3245.3166518211365	73	-95.10665	111.66718	8.957304	-516.67017	74.64179104477611	78.48950904183296	20004	1485412
3285.6689944267273	74	-85.04962	92.99898	8.16368	-463.36172	67.73986486486487	67.05123519013114	20051	1505463
3326.135573387146	75	-84.20157	96.29377	3.9304132	-435.963	67.14285714285714	73.69874156849801	20210	1525673
3366.623386144638	76	-88.542786	102.07889	8.914627	-449.29086	74.06273062730628	77.49719513027732	20071	1545744
3408.1736919879913	77	-99.144646	98.89267	2.211328	-456.8205	85.27234042553191	76.97605513882336	20039	1565783
3454.3933622837067	78	-100.815506	110.22928	26.014074	-475.04492	88.65065502183405	84.07145153054658	20301	1586084
3496.2716841697693	79	-93.15961	94.620804	24.836477	-461.48923	82.4320987654321	75.73507077291141	20031	1606115
3536.958359479904	80	-97.064384	108.23891	6.5714087	-438.0372	84.0798319327731	87.51867707493379	20011	1626126
3577.8074617385864	81	-99.27482	97.19987	2.8209562	-425.13232	87.62608695652175	79.51592248201835	20154	1646280
3618.838623523712	82	-91.10205	93.39027	6.3489943	-445.93512	84.49367088607595	79.27368712562094	20025	1666305
3659.169623851776	83	-98.73899	102.88228	8.9189005	-407.64667	93.7196261682243	91.21134894058515	20056	1686361
3700.1033968925476	84	-82.05225	90.83287	12.605424	-385.6164	82.34567901234568	83.20941312103066	20010	1706371
3743.105142354965	85	-74.776474	85.863304	9.674688	-359.01013	79.46825396825396	78.35690491427685	20026	1726397
3790.9287486076355	86	-72.10379	79.73189	16.908466	-349.82782	80.828	80.39053685602553	20207	1746604
3838.9294877052307	87	-69.34634	81.51823	6.613538	-395.78433	72.48913043478261	76.68132265533369	20007	1766611
3881.3583488464355	88	-72.57737	84.52243	31.453794	-368.03876	73.3003663003663	76.94875726928323	20011	1786622
3934.5528523921967	89	-68.99899	83.42469	17.961365	-351.33875	73.85661764705883	82.6170606243453	20089	1806711
3981.711895465851	90	-64.75104	74.58657	23.630539	-337.9197	72.77454545454546	77.32331229838378	20013	1826724
4023.3703899383545	91	-70.80881	72.922295	33.69676	-334.55957	84.97457627118644	79.43155692160356	20054	1846778
4064.2088599205017	92	-72.23002	81.677704	25.993645	-297.2121	97.6829268292683	91.88774674128085	20025	1866803
4105.177281141281	93	-84.57402	84.37491	13.994846	-320.29962	110.78688524590164	96.98613186099554	20274	1887077
4148.749875307083	94	-91.95942	89.25417	34.27034	-348.5525	123.33742331288343	102.0250312639189	20104	1907181
4206.452574491501	95	-71.87069	75.45468	30.167957	-294.573	105.62105263157895	90.03124357657512	20068	1927249
4248.785661458969	96	-71.919945	75.252	18.946602	-284.25848	106.70744680851064	89.53143123276651	20061	1947310
4304.3346247673035	97	-61.066597	69.40393	20.41594	-288.9126	92.40092165898618	86.50303315892641	20051	1967361
4346.856766700745	98	-62.328568	68.535324	36.422745	-268.646	95.07109004739337	86.72836970727593	20060	1987421
4402.577214956284	99	-71.04624	80.355576	28.906765	-292.18848	104.27083333333333	97.75282169485214	20020	2007441
4454.421334266663	100	-64.93712	73.06501	25.571482	-281.59888	101.74242424242425	96.85248242403911	20145	2027586
4506.120944976807	101	-77.26939	72.43934	22.647024	-252.58957	124.5031055900621	105.37699987263137	20045	2047631
4557.1124494075775	102	-72.75135	73.668076	22.377182	-297.13034	119.10714285714286	102.43331371435467	20010	2067641
4603.597656488419	103	-77.58815	70.419655	11.427147	-243.14833	123.89506172839506	104.3978979037633	20071	2087712
4644.886379957199	104	-72.60063	63.339462	18.807463	-283.69678	121.07185628742515	100.64971979175594	20219	2107931
4691.591019868851	105	-81.11779	63.10143	19.07477	-238.46414	140.5874125874126	104.14321289746152	20104	2128035
4739.366033792496	106	-64.100365	59.86244	20.91071	-260.09814	110.5303867403315	94.12355471285673	20006	2148041
4792.363225698471	107	-63.458504	63.061844	34.355442	-247.72943	124.32515337423312	100.04746624616281	20265	2168306
4839.913047552109	108	-58.847294	59.14847	26.234154	-272.69614	127.73248407643312	101.59212706880587	20054	2188360
4885.42028427124	109	-51.81668	51.329025	25.412199	-190.25304	129.2051282051282	101.04516113642518	20156	2208516
4925.727987766266	110	-43.056767	51.45467	33.0092	-217.2352	117.06432748538012	97.82208137565202	20018	2228534
4973.139308452606	111	-42.021484	55.122265	91.314224	-246.2422	115.92528735632185	95.85381676352704	20171	2248705
5021.137122392654	112	-39.669106	46.719387	38.396458	-195.94269	109.9010989010989	91.14335465459627	20002	2268707
5066.937408208847	113	-36.616436	50.864437	34.24836	-252.0793	99.51741293532338	93.94272069413654	20003	2288710
5109.416724681854	114	-30.272041	45.620766	37.176647	-198.25214	102.38383838383838	92.20373872990106	20272	2308982
5150.311545133591	115	-39.585014	47.95957	24.558825	-203.4543	123.03030303030303	98.78421075175713	20300	2329282
5190.6673328876495	116	-44.284416	44.306355	30.737446	-168.86993	150.2910447761194	103.98290794012524	20139	2349421
5233.414994478226	117	-41.54467	49.87454	36.85471	-194.86768	141.1118881118881	97.5347243092704	20179	2369600
5275.103287220001	118	-36.656467	46.294266	92.204575	-165.97629	136.70068027210885	101.48639632479326	20095	2389695
5317.007043123245	119	-29.977737	47.664963	54.7157	-223.36697	128.81528662420382	100.65769841594607	20224	2409919
5361.922980308533	120	-28.060633	46.309547	47.020374	-169.00662	117.84117647058824	99.87988997864049	20033	2429952
5402.773221731186	121	-23.861519	39.364532	69.09877	-176.46179	111.38333333333334	95.85905596818233	20049	2450001
5443.673394203186	122	-18.51314	37.92375	78.34712	-202.16057	83.04918032786885	80.80332857725729	20264	2470265
5484.585247755051	123	-21.93036	41.545452	80.71602	-192.68765	108.4972972972973	97.29348881435166	20072	2490337
5530.02235865593	124	-15.856175	34.623394	49.69726	-137.63168	106.57446808510639	91.60031001546793	20036	2510373
5571.42224240303	125	-21.111584	39.961723	61.63769	-150.80423	134.53691275167785	107.17967874918092	20046	2530419
5625.769737482071	126	-22.41283	36.78579	75.132256	-132.36801	146.06521739130434	108.02443406649961	20157	2550576
5679.421221971512	127	-15.387017	37.205723	123.833244	-153.66081	145.0863309352518	104.02894464497867	20167	2570743
5729.185531377792	128	-26.358315	42.79262	66.72341	-217.90329	161.73387096774192	104.76626125771043	20055	2590798
5788.496105670929	129	-22.349125	42.53318	88.15844	-177.42079	153.0916030534351	99.75377705310972	20055	2610853
5838.0528264045715	130	-22.815086	39.71526	64.99167	-124.396065	167.425	106.12411464098692	20091	2630944
5880.865298509598	131	-20.930204	39.828297	89.62131	-132.34741	158.21875	101.45749860625138	20252	2651196
5921.134492397308	132	-18.225761	34.876637	69.33393	-123.9468	132.98675496688742	100.82586041886704	20081	2671277
5961.714802026749	133	-18.247993	35.03437	78.45726	-142.29541	149.72592592592594	96.85425703408264	20213	2691490
6001.870381116867	134	-19.523968	36.462017	66.71358	-116.6804	157.51181102362204	109.81235721678866	20004	2711494
6050.310446023941	135	-14.315706	35.673187	61.906754	-171.58565	162.6991869918699	107.06611702108734	20012	2731506
6093.860444784164	136	-8.251623	33.496216	78.183044	-112.634384	132.50993377483445	100.34040522734432	20009	2751515
6136.514900684357	137	-5.451429	37.359943	171.18625	-96.42387	155.11627906976744	106.92978409797688	20010	2771525
6191.319436311722	138	-0.09401107	30.439798	118.02295	-90.91528	140.97202797202797	102.30927635730039	20159	2791684
6233.499092340469	139	-0.25818977	32.038097	110.67172	-105.30023	132.3421052631579	100.21825558980062	20116	2811800
6288.42095041275	140	0.3799962	32.430504	92.67612	-117.36615	129.7290322580645	100.103708137783	20108	2831908
6330.280082464218	141	-3.0714433	37.406116	102.63979	-122.054474	126.3375	100.72604476375511	20214	2852122
6371.23158788681	142	2.7780473	28.661282	103.38748	-91.84787	124.66459627329192	100.79374081543915	20071	2872193
6430.105697393417	143	7.0412884	31.572048	99.35966	-132.26523	134.64666666666668	104.10047304834349	20197	2892390
6492.823833703995	144	6.3992853	35.74948	112.19531	-126.74643	163.869918699187	104.97959223664498	20156	2912546
6539.828462123871	145	9.2908125	35.869133	140.01315	-115.285934	155.68992248062017	99.91490941421007	20084	2932630
6588.363274335861	146	16.000433	31.160728	115.82985	-83.42539	147.98529411764707	101.09976037777194	20126	2952756
6641.224500656128	147	16.627918	36.668232	142.58163	-131.15073	156.3046875	98.99254695696713	20007	2972763
6697.855538845062	148	16.662502	34.12299	123.23699	-108.80348	147.3065693430657	103.0993116322001	20181	2992944
6745.424787759781	149	16.876078	34.154957	113.72693	-65.61781	149.82089552238807	103.63986517228484	20076	3013020
6797.002144575119	150	13.341198	35.388966	219.40219	-72.276306	116.73255813953489	91.62270009405795	20078	3033098
6840.803225517273	151	12.405461	31.052078	135.30687	-111.556	113.12429378531074	93.76582399144715	20023	3053121
6895.6106133461	152	19.315628	34.01704	152.38509	-112.92959	121.87272727272727	94.627061517041	20109	3073230
6941.730858802795	153	14.530468	37.53636	139.85735	-102.71607	150.8345864661654	106.28421115395486	20061	3093291
6991.875080108643	154	15.06669	33.898357	135.71072	-114.1561	154.69230769230768	106.04874395613818	20110	3113401
7037.669153928757	155	16.008474	35.057976	103.28963	-102.50549	172.94827586206895	105.81985707890443	20062	3133463
7079.973291635513	156	18.162151	31.20321	108.65484	-68.67227	191.4811320754717	106.91622810724606	20297	3153760
7121.253794193268	157	21.67832	31.975105	106.211334	-60.522488	208.82291666666666	100.91095443422577	20047	3173807
7161.588153600693	158	34.132523	42.736885	172.69312	-85.32504	227.82954545454547	98.16581769348102	20049	3193856
7202.011692047119	159	35.722744	37.041138	124.528755	-46.15455	202.82828282828282	109.72509638108869	20080	3213936
7242.32186126709	160	33.294834	30.173141	102.745224	-53.831474	179.32142857142858	105.89088169682323	20084	3234020
7282.825856208801	161	32.51948	34.659206	164.80194	-65.68974	166.12295081967213	105.05046233688981	20267	3254287
7330.504371404648	162	33.61931	32.50616	120.42148	-37.80697	180.55855855855856	106.8652165057609	20042	3274329
7381.467001914978	163	31.921392	37.87776	134.5868	-71.88408	179.63716814159292	104.5837989743621	20299	3294628
7436.0212206840515	164	44.577408	42.235306	150.775	-127.639595	194.37864077669903	112.18795917815908	20021	3314649
7498.35543012619	165	39.786224	45.785286	175.86035	-82.94676	195.51456310679612	109.1594419576174	20138	3334787
7550.238660335541	166	32.232155	38.81084	171.70465	-73.63895	196.9313725490196	108.37944054759753	20087	3354874
7602.423633337021	167	29.350492	33.513943	157.71155	-37.813072	159.29365079365078	111.79183230098424	20071	3374945
7654.529754161835	168	38.722122	36.78885	142.23792	-25.779263	188.20560747663552	110.71025907242431	20138	3395083
7706.545850515366	169	50.8429	39.623356	145.52519	-31.785088	192.9047619047619	102.59216751032761	20255	3415338
7758.516232013702	170	67.70148	47.094055	207.32416	-77.793755	217.43010752688173	100.11685622305784	20221	3435559
7810.250270843506	171	70.10107	44.063828	196.37943	-30.055647	218.1086956521739	102.5174912458462	20066	3455625
7862.466214418411	172	61.379345	42.038944	196.84879	-5.517065	186.28703703703704	108.83312497222917	20119	3475744
7913.246669769287	173	61.251816	42.626118	158.9888	-7.0327716	187.05607476635515	111.74401920274389	20015	3495759
7964.040371656418	174	56.0469	49.388058	193.06032	-31.14442	191.13333333333333	105.61953883693577	20069	3515828
8015.341027975082	175	67.5087	46.33521	188.7073	1.5548391	211.30526315789473	107.90864300628132	20074	3535902
8065.933566808701	176	52.335938	47.7794	206.67578	-22.506063	168.4033613445378	105.14418426379885	20040	3555942
8116.998861312866	177	58.51398	45.08151	160.85126	-33.358498	187.02803738317758	110.03478277123385	20012	3575954
8169.2401287555695	178	71.70152	50.119385	210.42523	-9.781643	182.79090909090908	107.84543615366185	20107	3596061
8207.453206300735	179	79.65257	51.288765	210.39075	-35.47381	217.46236559139786	99.80330754315969	20224	3616285
8242.585929870605	180	85.1658	51.87829	213.06421	-20.541805	228.27272727272728	95.6400362239874	20088	3636373
8278.199558734894	181	88.40954	54.05676	227.56183	0.9455986	230.4090909090909	98.75240346484051	20276	3656649
8313.807959079742	182	95.300255	51.893955	285.71893	-5.276568	232.2183908045977	96.67856747441358	20203	3676852
8349.398803710938	183	90.29709	54.670677	211.67036	-7.6985583	216.36559139784947	103.97185154080066	20122	3696974
8384.462997436523	184	80.40765	47.194897	202.89569	-20.422142	219.85714285714286	100.02950898989664	20007	3716981
8419.89836525917	185	77.431564	56.135868	235.68373	-33.890114	193.69230769230768	110.54385458713773	20144	3737125
8454.782888889313	186	70.44488	47.98618	249.42285	-20.01376	205.03061224489795	107.58247468249107	20093	3757218
8498.138595342636	187	69.33008	44.896053	210.73705	-30.77803	211.44791666666666	106.22723969864347	20299	3777517
8551.31054854393	188	70.521774	44.89745	184.35822	-29.102884	211.1263157894737	106.02623727969296	20057	3797574
8603.136421203613	189	81.69083	51.601177	232.08185	0.53696823	216.78494623655914	106.18515473083873	20161	3817735
8654.435739040375	190	88.18686	47.974358	203.0798	-7.2305098	233.8139534883721	101.49103656468674	20108	3837843
8706.037581682205	191	87.843666	50.745277	243.07956	-9.7850895	236.41176470588235	97.0662956125587	20095	3857938
8758.006329536438	192	95.0907	53.16475	213.07986	-19.407726	241.3095238095238	94.5030833700145	20270	3878208
8809.324650764465	193	100.611786	52.496845	234.30524	-1.4312809	230.79310344827587	98.91075019512144	20079	3898287
8862.001689195633	194	96.90164	58.610813	257.70328	-25.670883	225.24719101123594	101.64581388760497	20047	3918334
8913.773386240005	195	100.88513	54.40006	276.35352	3.7676415	224.9777777777778	100.31693972137371	20248	3938582
8964.919473648071	196	67.641716	52.91114	225.06033	-17.351233	149.4402985074627	108.32741354469805	20025	3958607
9015.860684633255	197	63.82023	48.244774	245.53319	-12.755502	146.31386861313868	110.35792807684182	20045	3978652
9067.659678459167	198	77.56459	53.296993	216.32782	-8.268252	178.18584070796462	108.27673073649515	20135	3998787
9118.934725999832	199	109.26493	51.582962	215.59863	-3.6495445	233.30232558139534	97.92819744617373	20064	4018851
9171.875215053558	200	123.73344	46.983833	221.4055	4.4226046	270.31081081081084	70.75938017765822	20003	4038854
9223.435433864594	201	128.50319	46.702	226.06833	4.3650794	268.6933333333333	76.87114297460538	20152	4059006
9275.013193845749	202	119.554886	49.516785	216.97752	2.4663754	256.60759493670884	85.16810149308948	20272	4079278
9326.105571746826	203	125.42961	46.59159	251.03284	21.178368	260.42857142857144	76.35025724302483	20053	4099331
9372.926362037659	204	124.34325	43.59027	204.62741	19.905142	262.42857142857144	77.9512715875439	20207	4119538
11171.483288764954	205	118.38984	51.98166	244.07434	-11.4750395	241.14457831325302	94.17480958176628	20015	4139553
11223.489113092422	206	122.83534	61.84943	295.52225	-3.047397	225.23595505617976	99.93582462917183	20046	4159599
11280.58174443245	207	118.53739	55.35317	248.17152	5.6242747	241.15662650602408	93.08468510269415	20016	4179615
11334.648313045502	208	107.208824	58.899673	254.4032	0.4988377	209.78125	101.53798582355357	20139	4199754
11396.074112176895	209	120.284744	71.76208	282.66562	-6.548964	206.28571428571428	105.91862490629688	20216	4219970
11477.355103254318	210	124.064674	65.69832	267.8653	7.7295465	216.55913978494624	104.78420860727813	20140	4240110
11554.101504802704	211	136.88411	70.800766	318.7906	13.346285	220.47252747252747	99.22417073700868	20063	4260173
11629.28788781166	212	136.52493	59.93375	266.04196	4.278044	224.77528089887642	93.90752716704321	20005	4280178
11705.02133345604	213	126.78673	74.443855	301.0451	2.2643042	210.69473684210527	103.77435781887654	20016	4300194
11783.047771215439	214	108.09428	67.01351	284.89124	-14.462436	201.08	104.94414514397647	20108	4320302
11857.69000673294	215	112.6101	62.276264	253.93454	7.566125	205.01020408163265	103.95254950667608	20091	4340393
11935.588284730911	216	111.63688	63.279995	300.78067	11.36291	179.39285714285714	99.14854990860728	20092	4360485
12011.354041099548	217	130.40279	67.59123	288.43567	8.194235	218.27173913043478	100.05575654960865	20081	4380566
12089.13004040718	218	129.10489	65.31271	290.6222	0.5208897	224.8	101.12722460126925	20232	4400798
12166.327031612396	219	136.50867	66.28383	287.47656	5.8025236	220.41758241758242	98.58783844197411	20058	4420856
12242.061566352844	220	144.75433	76.17331	284.05823	0.9535961	224.97752808988764	107.37469598424893	20023	4440879
12317.78305888176	221	140.05322	63.607784	248.40746	20.11147	226.03370786516854	100.32558787707626	20117	4460996
12393.67323255539	222	157.81123	65.59069	302.33844	9.485347	248.30864197530863	85.45400061794976	20113	4481109
12468.919425010681	223	141.95566	61.784924	259.39932	5.7755785	241.14457831325302	92.83410940056287	20015	4501124
12544.492872953415	224	140.75351	64.46643	258.65366	5.2736826	241.5	98.96157262868682	20286	4521410
12620.510880470276	225	139.29912	52.958565	224.38896	14.173942	258.79487179487177	79.40247347667662	20186	4541596
12696.28445315361	226	153.97871	48.390865	315.9635	24.320013	272.13513513513516	68.55542723490328	20138	4561734
12771.98251080513	227	151.28696	51.380478	251.93784	12.485745	264.10526315789474	77.40657848885843	20072	4581806
12847.980773925781	228	152.63852	53.62971	263.32983	14.420772	265.9736842105263	80.77714203845217	20214	4602020
12925.404570817947	229	160.78407	56.8698	295.52048	16.645071	258.34615384615387	83.98547293844501	20151	4622171
13002.227448225021	230	172.33084	55.892548	281.76123	5.6814594	273.5405405405405	71.8226398826151	20242	4642413
13078.333779335022	231	169.62076	55.33917	284.37674	20.177841	272.9189189189189	69.59899749914017	20196	4662609
13154.395518541336	232	158.99812	68.08515	283.91382	7.6700063	254.83544303797467	91.57945023757445	20132	4682741
13231.689686536789	233	165.54967	49.47235	277.1538	35.498016	269.12	69.21001083658345	20184	4702925
13309.441179513931	234	161.3773	55.608208	274.221	15.530799	270.3466666666667	73.82113398087448	20276	4723201
13385.329438447952	235	150.97337	56.212566	276.0668	24.876427	265.9605263157895	77.66229878360909	20213	4743414
13462.488431692123	236	144.62979	58.988903	260.9005	8.419776	256.54430379746833	88.28159741552496	20267	4763681
13538.96736741066	237	156.59927	54.804283	275.32837	17.164614	268.5733333333333	78.24204723520184	20143	4783824
13616.484811306	238	163.3526	58.381786	290.12177	1.930495	269.52	79.04452078839282	20214	4804038
13694.267500162125	239	145.35834	61.74935	286.79126	11.782014	235.24418604651163	99.43888342074938	20231	4824269
13770.161133766174	240	167.10858	51.583374	283.80298	18.54422	261.2987012987013	74.97048162574715	20120	4844389
13847.636178016663	241	161.31467	58.49089	290.90012	8.3531475	259.29487179487177	84.21281176881108	20225	4864614
13925.401567697525	242	174.96048	57.991028	318.50177	27.71282	268.7733333333333	70.94083418987653	20158	4884772
14000.765845537186	243	167.89241	61.14912	312.05292	9.495821	252.0375	88.83544390472757	20163	4904935
14077.827311992645	244	168.56827	66.11079	291.1507	7.9420986	244.53012048192772	92.2862672170294	20296	4925231
14154.893396615982	245	153.90059	71.93436	313.6447	-10.386057	235.41860465116278	106.1142939230959	20246	4945477
14231.858479261398	246	150.1037	70.148674	292.77847	14.112129	223.82222222222222	106.2973374577994	20144	4965621
14306.758377075195	247	153.77695	70.71232	297.85336	4.6627107	230.82758620689654	102.5509356925376	20082	4985703
14382.508316993713	248	162.84898	80.308914	294.67377	5.4871616	232.97701149425288	105.79039969170597	20269	5005972
14459.140286922455	249	168.41743	70.89358	296.71692	6.920353	244.97560975609755	97.18927668563532	20088	5026060
14537.41944861412	250	179.55391	65.076706	293.00296	8.219183	252.275	87.31522991437404	20182	5046242
14614.344987392426	251	154.5973	74.9781	291.25436	11.608325	220.27472527472528	109.13708145264583	20045	5066287
14689.407384634018	252	170.53658	69.87502	318.75992	-1.6761876	242.44578313253012	92.80946319272428	20123	5086410
14764.426584482193	253	149.93436	76.0286	304.52527	-1.6946021	217.05376344086022	112.3255986863418	20186	5106596
14835.012571811676	254	164.49995	65.35349	331.86517	15.36728	236.72941176470587	93.57279496989233	20122	5126718
14907.345257520676	255	171.79453	56.118053	265.36234	25.905706	256.17721518987344	83.19289153750708	20238	5146956
14980.177916049957	256	181.27039	49.53992	308.97156	1.135625	264.5263157894737	74.94427292542993	20104	5167060
15053.19755768776	257	189.32982	55.631363	298.34534	24.267357	269.76	73.61029638123551	20232	5187292
15126.668551683426	258	188.21233	61.074398	318.26877	20.002172	273.9189189189189	74.59381119839323	20270	5207562
15199.318949222565	259	214.74435	47.632828	303.69604	11.908305	288.4714285714286	46.113438211365995	20193	5227755
15271.355038166046	260	209.71538	50.276566	319.9657	50.835293	286.7	46.0953514854961	20069	5247824
15343.01850104332	261	218.0965	65.606186	340.89508	31.349113	273.56756756756755	68.81253049803307	20244	5268068
15388.465821504593	262	228.91933	62.63794	340.7074	44.5783	276.5753424657534	60.01139567768847	20190	5288258
15429.912162303925	263	224.05276	49.352737	336.26544	59.598125	280.8611111111111	49.44112816594083	20222	5308480
15470.520420074463	264	203.21965	76.18561	343.04395	15.494665	258.4871794871795	88.02137613186508	20162	5328642
15511.376026630402	265	191.00864	74.754425	334.213	1.1542019	257.4230769230769	88.28298311951893	20079	5348721
15551.98933339119	266	172.91927	79.22857	306.18848	3.1968808	239.47619047619048	102.51172710968896	20116	5368837
15592.227063655853	267	174.78734	76.80385	348.50977	11.294635	235.31764705882352	102.03312170013928	20002	5388839
15632.964906692505	268	187.3903	74.63076	297.3366	11.670809	246.76829268292684	96.68875117821175	20235	5409074
15673.149752140045	269	185.44034	83.87667	327.2353	10.822588	238.57142857142858	100.60203132881063	20040	5429114
15713.344496011734	270	199.48874	72.26576	334.38666	19.341822	260.6753246753247	82.75365437976909	20072	5449186
15754.484246015549	271	213.22737	67.86604	322.0215	23.172783	265.89473684210526	72.89115514666342	20208	5469394
15794.6529443264	272	219.22876	66.08251	324.51862	12.9430275	271.1216216216216	73.00258283152074	20063	5489457
15835.387610912323	273	213.1165	63.494595	316.56494	21.8948	265.17105263157896	72.44769222976176	20153	5509610
15876.076570987701	274	217.52347	68.57691	346.8283	17.75762	273.3918918918919	70.3412180821857	20231	5529841
15916.754473686218	275	234.73714	54.997566	341.264	47.69806	288.1714285714286	45.7102587512042	20172	5550013
15957.608402252197	276	217.9895	66.336845	326.19785	7.6328006	269.49333333333334	75.21354015217088	20212	5570225
15998.3339574337	277	212.79816	75.27621	355.69928	11.717065	256.8481012658228	82.70767319454445	20291	5590516
16038.405241966248	278	214.29608	70.361786	349.83075	20.948559	270.63513513513516	75.9551030836059	20027	5610543
16078.7827398777	279	198.67989	78.02997	335.9654	12.858649	254.31645569620252	89.15603507317694	20091	5630634
16119.140597343445	280	198.04727	68.081856	321.66583	13.498008	264.0263157894737	78.20855614859315	20066	5650700
16159.563035964966	281	204.98972	75.973434	325.88144	9.154448	260.85714285714283	79.91435582648096	20086	5670786
16200.368855953217	282	166.0277	84.22656	333.08743	3.757079	219.94505494505495	112.96818210387437	20015	5690801
16241.360422849655	283	154.61531	75.72523	307.40448	10.953545	209.23711340206185	107.1757178603626	20296	5711097
16281.997253417969	284	167.88405	82.440414	306.4995	2.5547757	223.76666666666668	105.47849175174161	20139	5731236
16322.784948825836	285	171.44261	77.94365	350.57086	4.5154543	238.74117647058824	98.5422158493418	20293	5751529
16363.459524393082	286	175.14226	76.48697	314.7434	4.4995575	238.1904761904762	98.25022430490284	20008	5771537
16404.119100809097	287	175.69011	71.039085	309.60126	34.732124	249.39506172839506	88.5802529358625	20201	5791738
16444.8942797184	288	169.94096	67.00312	270.88837	20.969276	243.65060240963857	95.66236229509528	20223	5811961
16485.341290473938	289	176.7236	66.28672	299.27698	15.877339	247.2560975609756	88.32793815063103	20275	5832236
16525.2520570755	290	189.90686	75.603874	304.30334	7.4292436	247.1851851851852	96.79800925295974	20022	5852258
16565.9251101017	291	216.60873	67.04848	326.64136	19.635307	269.52	75.29450798918427	20214	5872472
16607.053291797638	292	226.97925	63.873592	326.37244	14.969036	269.7866666666667	73.92487958882465	20234	5892706
16647.310128211975	293	228.75128	72.48119	358.91913	40.796707	266.8666666666667	72.44314245592118	20015	5912721
16687.50665950775	294	242.03044	69.46402	404.93954	37.777767	275.35616438356163	65.16015461669507	20101	5932822
16728.023298978806	295	214.51648	89.652054	384.69592	10.348855	242.85542168674698	94.88396623728671	20157	5952979
16768.101851701736	296	214.19232	100.721245	390.26163	4.233669	235.68235294117648	98.8922422181551	20033	5973012
16808.4097969532	297	205.21367	107.23376	405.24283	8.913472	216.69892473118279	102.98123693243407	20153	5993165
16848.699365377426	298	172.78688	89.21426	392.4612	9.132451	209.32291666666666	109.17956299063663	20095	6013260
16888.908420562744	299	183.83794	88.08639	339.29333	12.580147	220.30769230769232	108.75435495210378	20048	6033308
