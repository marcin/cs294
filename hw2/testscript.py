""" This script will test some of the functionality """

import train_pg_f18 as trainer
import tensorflow as tf

with tf.Session():
    input_ph = tf.placeholder(dtype = tf.float32, shape = (100, 20))
    trainer.build_mlp(input_ph, 5, 'testscope', 4, 50, activation = tf.nn.relu)
    writer = tf.summary.FileWriter('./graphs', tf.get_default_session().graph)
